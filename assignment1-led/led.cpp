#include "mbed.h"

#define ON 1
#define OFF 0
#define MAX 20

DigitalOut one(LED1); //LED1 output obect created
DigitalOut two(LED2);
DigitalOut three(LED3);
DigitalOut four(LED4);
Timer tim;              //timer object created

int main() 
{
    tim.start();       //timer started
    one = OFF;			//LED initially OFF
    two = OFF;
    three = OFF;
    
    while(1)
    {
        tim.reset();    //timer is reset
        four = OFF; 
        one = ON;               //LED1 is ON
        while(tim.read() <= 1);
        
        tim.reset();        //timer is reset
        one = OFF;              //LED1 is OFF
        two = ON;
        while(tim.read() <= 1);   //wait for 1 sec
        
        tim.reset(); 
        two = OFF;
        three = ON;
        while(tim.read() <= 1);
        
        tim.reset();
        three = OFF;
        four = ON;
        while(tim.read() <= 1);
    }
    
    tim.stop();    //timer stop
    return 0;
}
