#include "mbed.h"           
 
Serial comp(USBTX, USBRX);   //serial object created so as to communicate with PC terminal
 
int main()
{
    while(1)
    {
        comp.printf("Hello World...\n");  //message to be printed on PC terminal
    }
    
    return 0;
}